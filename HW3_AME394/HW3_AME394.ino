#include <Arduino.h>

extern "C" {
#include "user_interface.h"
}

char * HOSTNAME = "HTTP";
char * WifiPASS = "=";

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Hash.h>



ESP8266WebServer server (80);


void setup() {
  Serial.begin(115200);

  pinMode(D6, OUTPUT);

  // Start Wifi AP
  WiFi.mode(WIFI_AP_STA);
  WiFi.softAP(HOSTNAME, WifiPASS);

  // handle index -- HTTP Server

  server.on("/10", []() { // so i changed "on" into a "#" 10 and also did the same when a href was referencing and priniting out 10
    analogWrite(D6, 25.5); // I also chnages this to anlogwrite, so it can refer to pin D6 and controls the LED brightness
    server.send(200, "text/html", "<html><head></head><body>  <a href=\"./10\">10</a>  <br>  <a href=\"./20\">20</a>  <br>  <a href=\"./30\">30</a>  <br>  <a href=\"./40\">40</a>  <br>  <a href=\"./50\">50</a>  <br>  <a href=\"./60\">60</a>  <br>  <a href=\"./70\">70</a>  <br>  <a href=\"./80\">80</a> <br> <a href=\"./90\">90</a> <br> <a href=\"./100\">100</a> </body><html>");
  }); // I divided 255 by 10 and set each method (10) by its corresponding led value brightness.

  server.on("/20", []() {
    analogWrite(D6, 51);
    server.send(200, "text/html", "<html><head></head><body>  <a href=\"./10\">10</a>  <br>  <a href=\"./20\">20</a>  <br>  <a href=\"./30\">30</a>  <br>  <a href=\"./40\">40</a>  <br>  <a href=\"./50\">50</a>  <br>  <a href=\"./60\">60</a>  <br>  <a href=\"./70\">70</a>  <br>  <a href=\"./80\">80</a> <br> <a href=\"./90\">90</a> <br> <a href=\"./100\">100</a> </body><html>");
  });

  server.on("/30", []() {
    analogWrite(D6, 76.5);
    server.send(200, "text/html", "<html><head></head><body>  <a href=\"./10\">10</a>  <br>  <a href=\"./20\">20</a>  <br>  <a href=\"./30\">30</a>  <br>  <a href=\"./40\">40</a>  <br>  <a href=\"./50\">50</a>  <br>  <a href=\"./60\">60</a>  <br>  <a href=\"./70\">70</a>  <br>  <a href=\"./80\">80</a> <br> <a href=\"./90\">90</a> <br> <a href=\"./100\">100</a> </body><html>");
  });

  server.on("/40", []() {
    analogWrite(D6, 102);
    server.send(200, "text/html", "<html><head></head><body>  <a href=\"./10\">10</a>  <br>  <a href=\"./20\">20</a>  <br>  <a href=\"./30\">30</a>  <br>  <a href=\"./40\">40</a>  <br>  <a href=\"./50\">50</a>  <br>  <a href=\"./60\">60</a>  <br>  <a href=\"./70\">70</a>  <br>  <a href=\"./80\">80</a> <br> <a href=\"./90\">90</a> <br> <a href=\"./100\">100</a> </body><html>");
  });

  server.on("/50", []() {
    analogWrite(D6, 127.5);
    server.send(200, "text/html", "<html><head></head><body>  <a href=\"./10\">10</a>  <br>  <a href=\"./20\">20</a>  <br>  <a href=\"./30\">30</a>  <br>  <a href=\"./40\">40</a>  <br>  <a href=\"./50\">50</a>  <br>  <a href=\"./60\">60</a>  <br>  <a href=\"./70\">70</a>  <br>  <a href=\"./80\">80</a> <br> <a href=\"./90\">90</a> <br> <a href=\"./100\">100</a> </body><html>");
  });

  server.on("/60", []() {
    analogWrite(D6, 153);
    server.send(200, "text/html", "<html><head></head><body>  <a href=\"./10\">10</a>  <br>  <a href=\"./20\">20</a>  <br>  <a href=\"./30\">30</a>  <br>  <a href=\"./40\">40</a>  <br>  <a href=\"./50\">50</a>  <br>  <a href=\"./60\">60</a>  <br>  <a href=\"./70\">70</a>  <br>  <a href=\"./80\">80</a> <br> <a href=\"./90\">90</a> <br> <a href=\"./100\">100</a> </body><html>");
  });

  server.on("/70", []() {
    analogWrite(D6, 178.5);
    server.send(200, "text/html", "<html><head></head><body>  <a href=\"./10\">10</a>  <br>  <a href=\"./20\">20</a>  <br>  <a href=\"./30\">30</a>  <br>  <a href=\"./40\">40</a>  <br>  <a href=\"./50\">50</a>  <br>  <a href=\"./60\">60</a>  <br>  <a href=\"./70\">70</a>  <br>  <a href=\"./80\">80</a> <br> <a href=\"./90\">90</a> <br> <a href=\"./100\">100</a> </body><html>");
  });

  server.on("/80", []() {
    analogWrite(D6, 204);
    server.send(200, "text/html", "<html><head></head><body>  <a href=\"./10\">10</a>  <br>  <a href=\"./20\">20</a>  <br>  <a href=\"./30\">30</a>  <br>  <a href=\"./40\">40</a>  <br>  <a href=\"./50\">50</a>  <br>  <a href=\"./60\">60</a>  <br>  <a href=\"./70\">70</a>  <br>  <a href=\"./80\">80</a> <br> <a href=\"./90\">90</a> <br> <a href=\"./100\">100</a> </body><html>");
  });

  server.on("/90", []() {
    analogWrite(D6, 229.5);
    server.send(200, "text/html", "<html><head></head><body>  <a href=\"./10\">10</a>  <br>  <a href=\"./20\">20</a>  <br>  <a href=\"./30\">30</a>  <br>  <a href=\"./40\">40</a>  <br>  <a href=\"./50\">50</a>  <br>  <a href=\"./60\">60</a>  <br>  <a href=\"./70\">70</a>  <br>  <a href=\"./80\">80</a> <br> <a href=\"./90\">90</a> <br> <a href=\"./100\">100</a> </body><html>");
  });

  server.on("/100", []() {
    analogWrite(D6, 255);
    server.send(200, "text/html", "<html><head></head><body>  <a href=\"./10\">10</a>  <br>  <a href=\"./20\">20</a>  <br>  <a href=\"./30\">30</a>  <br>  <a href=\"./40\">40</a>  <br>  <a href=\"./50\">50</a>  <br>  <a href=\"./60\">60</a>  <br>  <a href=\"./70\">70</a>  <br>  <a href=\"./80\">80</a> <br> <a href=\"./90\">90</a> <br> <a href=\"./100\">100</a> </body><html>");
  });

  server.on("/", []() {
    server.send(200, "text/html", "<html><head></head><body>  <a href=\"./10\">10</a>  <br>  <a href=\"./20\">20</a>  <br>  <a href=\"./30\">30</a>  <br>  <a href=\"./40\">40</a>  <br>  <a href=\"./50\">50</a>  <br>  <a href=\"./60\">60</a>  <br>  <a href=\"./70\">70</a>  <br>  <a href=\"./80\">80</a> <br> <a href=\"./90\">90</a> <br> <a href=\"./100\">100</a> </body><html>");
  });


  server.begin();

}

void loop() {
  server.handleClient();
}

