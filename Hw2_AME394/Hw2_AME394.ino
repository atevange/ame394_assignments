int led1 = D8;           
int led2 = D7;           
int led3 = D6;           
int led4 = D5;           

// the setup routine runs once when you press reset:
void setup() {                
  Serial.begin(115200);
  // initialize the digital pin as an output.
  pinMode(led1, OUTPUT); 
  pinMode(led2, OUTPUT);     
  pinMode(led3, OUTPUT);   
  pinMode(led4, OUTPUT);     
}
  
// the loop routine runs over and over again forever:
void loop() {
  int potPin = analogRead(A0); // A0 is analog input for potentiometer

  
  int brightness = map(potPin, 0, 256, 0, 1024); //map the max and min of each led
  analogWrite(led1, brightness); // this will light up the LED and control its brightnesss

  int brightness2 = map(potPin, 256, 512, 0, 1024);
  analogWrite(led2, brightness2);

   int brightness3 = map(potPin, 512, 708, 0, 1024);
  analogWrite(led3, brightness3);

   int brightness4 = map(potPin, 708, 1024, 0, 1024);
  analogWrite(led4, brightness4);


  
  Serial.println(potPin);
  delay(100);               // wait for a second
}

