#include <Servo.h>

#include <Wire.h> 

#include <SimpleDHT.h>
int pinDHT11 = D4;
SimpleDHT11 dht11(pinDHT11);

int pinServo = D6;

Servo servo;

int led1 = D7;
int led2 = D8;

const int pushButton = D5;

int lastButtonState = 0;


#include <ESP8266WiFi.h>
const char* ssid     = "Other...";
const char* password = "Bet@B0ys";


const char* host = "34.210.90.229";


void setup()
{
  Serial.begin(115200);

  pinMode(pushButton, INPUT);
  servo.attach(pinServo);

  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
}



float t = 0;
float h = 0;

int state = 0;

void loop()
{

  byte temperature = 0;
  byte humidity = 0;
  int err = SimpleDHTErrSuccess;
  if ((err = dht11.read(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT11 failed, err="); Serial.println(err);delay(1000);
    return;
  }
  
  Serial.print("Sample OK: ");
  Serial.print((int)temperature); Serial.print(" *C, "); 
  Serial.print((int)humidity); Serial.println(" H");

  t = (float)temperature*1.8+32;
  h = (float)humidity;
  



  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  // We now create a URI for the request
  String path = "/setValue?t=" + String(t) + "&h=" + String(h);


  Serial.print("Requesting URL: ");
  Serial.println(path);

  // This will send the request to the server
  client.print(String("GET ") + path + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }

  // Read all the lines of the reply from server and print them to Serial
  while (client.available()) {
    String line = client.readStringUntil('\r');
    Serial.println(line.length());
    Serial.print(line);
    int val = line.toInt();
  }

  Serial.println();
  Serial.println("closing connection");


int buttonState = digitalRead(pushButton);
if(buttonState == 1 && lastButtonState == 0) {
  lastButtonState = 1;
  state = state + 1;
  Serial.print("state = ");
  Serial.println(state);
  if(state > 1) {
    state = 0;
  }
}
if(buttonState == 0) {
  lastButtonState = 0;
}
int mappedT = 0;
int mappedH = 0;
  switch (state) {
    case 0:  
   mappedT = map(t, 0, 120, 40, 140);
   servo.write(mappedT);
   analogWrite(led1, 100);
   analogWrite(led2, 0);
   Serial.println("Temperature Displayed");
   Serial.print("Angle of servo is");
   Serial.println(mappedT);
   break;
    case 1:mappedH = map(h, 0, 100, 40, 140);
   servo.write(mappedH);
      analogWrite(led1, 0);
   analogWrite(led2, 100);
   Serial.println("Humidity Displayed");
   Serial.print("Angle of servo is");
   Serial.println(mappedH);
   break;
    default:mappedT = map(t, 60, 100, 40, 140);
   servo.write(mappedT);
   analogWrite(led1, 100);
   analogWrite(led2, 0);
   Serial.println("Temperature Displayed");
   Serial.print("Angle of servo is");
   Serial.println(mappedT);
   break;
  }


  Serial.print(buttonState);
  // DHT11 sampling rate is 1HZ.
  delay(200);  
}

