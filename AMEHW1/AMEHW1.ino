String storeData;

void setup() {
  Serial.begin(115200); // bits per second
  pinMode(D6, OUTPUT); // writing into LED 
}

void loop() {
  
    while (Serial.available() > 0) // while this data is coming in, this code executes
    {
        char recieved = Serial.read(); //input from serial monitor
        storeData += recieved; 

        // Process message when new line character is recieved
        if (recieved == '\n')
        {
            Serial.println("LED brightness at: " + storeData);
            //Serial.println(storeData);
            
            int x = constrain(storeData.toInt(), 0, 100); // constrain will limit what the input can be (range of 0-100)
            
            int brightness = map(x, 0, 100, 0, 1024); // maping the 0-100 for 0-1024
            
            analogWrite(D6, brightness); // pin and brightness 
            
            storeData = ""; // Clear recieved buffer
        }
    }
}


